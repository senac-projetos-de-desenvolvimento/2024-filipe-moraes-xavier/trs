import Phaser from 'phaser';
import PreloadScene from './scenes/PreloadScene';
import PlayScene from './scenes/PlayScene';
import GameScene from './scenes/GameScene';
import PauseScene from './scenes/PauseScene';

const config: Phaser.Types.Core.GameConfig = {
  title: 'Taticas de Senescau',
  type: Phaser.AUTO,
  width: 1400,
  height: 800,
  pixelArt: true,
  transparent: true,
  physics: {
    default: 'arcade',
    arcade: {
      debug: true,
    }
  },
  scene: [PreloadScene, PlayScene, GameScene, PauseScene],
};

new Phaser.Game(config);

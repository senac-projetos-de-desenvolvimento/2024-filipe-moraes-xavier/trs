import axios from 'axios';

const api = axios.create({
  baseURL: 'http://localhost:3004', // Replace with your backend URL
});

export const getChampions = async () => {
  try {
    const response = await api.get('/personagens');
    return response.data;
  } catch (error) {
    console.error('Error fetching champions:', error);
    throw error;
  }
};

// export const saveGameProgress = async (progress: any) => {
//   try {
//     const response = await api.post('/save', progress);
//     return response.data;
//   } catch (error) {
//     console.error('Error saving game progress:', error);
//     throw error;
//   }
// };

// Add other API methods as needed

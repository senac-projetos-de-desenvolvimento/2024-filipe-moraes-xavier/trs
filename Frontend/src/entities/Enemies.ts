import Phaser from 'phaser';

export class Enemies extends Phaser.Physics.Arcade.Sprite {
    constructor(scene: Phaser.Scene, x: number, y: number) {
        super(scene, x, y, 'enemy');
        
        // Add this sprite to the scene
        scene.add.existing(this);
        scene.physics.add.existing(this);
        
        // Set enemy properties
        this.setCollideWorldBounds(true);
        this.setVelocityX(-100);

        // Play fly animation
        this.play('enemy-fly');
    }
}
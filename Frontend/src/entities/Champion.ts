import Phaser from 'phaser';

export class Champion extends Phaser.Physics.Arcade.Sprite {
    constructor(scene: Phaser.Scene, x: number, y: number) {
        super(scene, x, y, 'champion');
        
        // Add this sprite to the scene
        scene.add.existing(this);
        scene.physics.add.existing(this);
        
        // Set champion properties
        this.setCollideWorldBounds(true);
        this.setBounce(0.2);

        // Play run animation
        this.play('champion-run');
    }

    update(cursors: Phaser.Types.Input.Keyboard.CursorKeys) {
        if (cursors.left.isDown) {
            this.setVelocityX(-160);
            this.anims.play('champion-run', true);
        } else if (cursors.right.isDown) {
            this.setVelocityX(160);
            this.anims.play('champion-run', true);
        } else {
            this.setVelocityX(0);
            this.anims.stop();
        }

        if (cursors.up.isDown && this.body.touching.down) {
            this.setVelocityY(-330);
        }
    }
}
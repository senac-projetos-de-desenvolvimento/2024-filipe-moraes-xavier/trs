import Phaser from 'phaser';
import BaseScene from './BaseScene';

class PauseScene extends BaseScene {
    private resumeButton!: Phaser.GameObjects.Text;
    private saveButton!: Phaser.GameObjects.Text;
    private settingsButton!: Phaser.GameObjects.Text;
    private exitButton!: Phaser.GameObjects.Text;

    constructor() {
        super('PauseScene');
    }

    create() {
        const width = this.sys.game.config.width as number;
        const height = this.sys.game.config.height as number;

        // Add a background overlay
        this.add.rectangle(width / 2, height / 2, width, height, 0x000000, 0.5);

        // Create buttons
        this.resumeButton = this.add.text(width / 2, height / 2 - 100, 'Continuar', { fontSize: '32px', color: '#ffffff' })
            .setOrigin(0.5)
            .setInteractive();

        this.saveButton = this.add.text(width / 2, height / 2 - 50, 'Salvar', { fontSize: '32px', color: '#ffffff' })
            .setOrigin(0.5)
            .setInteractive();

        this.settingsButton = this.add.text(width / 2, height / 2, 'Configurações', { fontSize: '32px', color: '#ffffff' })
            .setOrigin(0.5)
            .setInteractive();

        this.exitButton = this.add.text(width / 2, height / 2 + 50, 'Sair', { fontSize: '32px', color: '#ffffff' })
            .setOrigin(0.5)
            .setInteractive();

        // Add button click events
        this.resumeButton.on('pointerdown', () => {
            this.scene.resume('PlayScene');
            this.scene.stop();
        });

        this.exitButton.on('pointerdown', () => {
            this.scene.stop('PlayScene');
            this.scene.start('MenuScene'); // Replace 'MenuScene' with your main menu scene
        });

        // Placeholder for save and settings buttons
        this.saveButton.on('pointerdown', () => {
            // Implement save functionality here
        });

        this.settingsButton.on('pointerdown', () => {
            // Implement settings functionality here
        });
    }
}

export default PauseScene;

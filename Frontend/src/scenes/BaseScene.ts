import Phaser from 'phaser';

class BaseScene extends Phaser.Scene {
    constructor(key: string) {
        super(key);
    }

    create() {
        // Configurações e elementos comuns a todas as cenas
        this.add.image(0, 0, 'bgBridge').setOrigin(0);
    }
}

export default BaseScene;

import Phaser from 'phaser';
import BaseScene from './BaseScene';

class PlayScene extends BaseScene {
    private champions: Phaser.GameObjects.Sprite[] = [];
    private scrollContainer!: Phaser.GameObjects.Container;
    private circles: Phaser.GameObjects.Image[] = [];
    private enemies: Phaser.GameObjects.Sprite[] = [];
    enem: Phaser.Physics.Arcade.Group;
    anime: Phaser.Physics.Arcade.Group;

    constructor() {
        super("PlayScene");
    }

    create() {
        super.create();
        this.createBackground();
        this.createFormation();
        this.createChampionSelectionArea();
        this.createChampions();
        this.createAnimations();
        this.createEnemies(); // Create enemies

        // Start spawning enemies
        this.spawnEnemyLoop();
    }

    createBackground() {
        const width = this.sys.game.config.width as number;
        const height = this.sys.game.config.height as number;

        // Add background image
        this.add.image(0, 0, 'bgBridge').setOrigin(0, 0).setDisplaySize(width, height);
    }

    createFormation() {
        const circleKey = 'circle';
        const startX = 100;
        const startY = ((this.sys.game.config.height as number) / 2) - 70;
        const spacing = 180;
        const rows = 4;
    
        for (let row = 0; row < rows; row++) {
            for (let col = 0; col <= row; col++) {
                let x, y;
                if (col > 0) {
                    let startYy = startY + (-40 * col);
                    x = startX + col * (spacing - 80);
                    y = startYy + row * spacing - (spacing * row / 2);
                } else {
                    x = startX + col * spacing;
                    y = startY + row * spacing - (spacing * row / 2);
                }
    
                const circle = this.physics.add.sprite(x, y, circleKey).setScale(0.7);
                circle.setData('occupied', false); // Mark the circle as not occupied
                circle.setData('id', `circle-${row}-${col}`); // Add a unique identifier
    
                // Set collision box size
                circle.body.setSize(72, 72);
    
                this.circles.push(circle);
            }
        }
    }
    
    

    createChampionSelectionArea() {
        const width = this.sys.game.config.width as number;
        const height = this.sys.game.config.height as number;
        const selectionAreaHeight = 150;

        // Add the brown UI background image
        this.add.image(width / 2, height - selectionAreaHeight / 2, 'championSelectUIBrown').setDisplaySize(width, selectionAreaHeight);

        // Create a container for the champions
        this.scrollContainer = this.add.container(0, ((height - selectionAreaHeight / 2) - 10));
    }

    createChampions() {
        this.anime = this.physics.add.group();
    
        const champions = [
            'champion1', 'champion2', 'champion3', 'champion4', 'champion5', 'champion6'
        ];
    
        const startX = 150; // Adjust the initial position as needed
        const startY = 0; // Adjust the Y position to center in the selection area
        const spacing = 100; // Adjust the spacing between champions
    
        champions.forEach((championKey, index) => {
            // Create a container for each champion
            const championContainer = this.add.container(startX + index * spacing, startY);
    
            // Create a frame for the champion
            const frame = this.add.sprite(0, 0, 'frameChamps').setScale(1.5);
    
            // Create a sprite for the champion portrait
            const portrait = this.add.sprite(0, 0, championKey).setScale(0.2); // Adjust the scale as needed
    
            // Check if portrait is null
            if (!portrait) {
                console.error(`Failed to create portrait sprite for ${championKey}`);
                return; // Skip to the next champion
            }
    
            // Store initial positions for resetting after dragging
            portrait.setData('initialX', startX + index * spacing);
            portrait.setData('initialY', startY);
    
            // Create a level-up button for the champion
            const levelUpButton = this.add.sprite(0, 60, 'levelUpButton').setScale(0.5).setInteractive(); // Adjust Y position to add spacing
    
            // Add frame, portrait, and level-up button to the container
            championContainer.add([frame, portrait, levelUpButton]);
    
            // Add the champion container to the scroll container
            this.scrollContainer.add(championContainer);
    
            // Store the portrait sprites in an array for future reference
            this.champions.push(portrait);
    
            // Store sprite key for reference
            portrait.setData('sprite', `${championKey}Sprite`); // Assuming sprite key is championKey + 'Sprite'
    
            // Add level-up button click handler
            levelUpButton.on('pointerdown', () => {
                levelUpButton.setTexture('levelUpButtonPressed');
                // Implement level-up logic here
            });
    
            levelUpButton.on('pointerup', () => {
                levelUpButton.setTexture('levelUpButton');
            });
    
            // Make the portrait draggable
            portrait.setInteractive(); // Ensure the portrait is interactive before setting it draggable
            this.input.setDraggable(portrait);
        });
    
        this.setupDragging();
    }
    
    createEnemies() {
        const enemies = [
            'enemy1', 'enemy2', 'enemy3'
        ];

        enemies.forEach(enemyKey => {
            this.createEnemy(enemyKey);
        });
    }

    createEnemy(enemyKey: string) {
        this.enem = this.physics.add.group();
        const startX = this.sys.game.config.width as number + 50; // Start outside the right edge of the screen
        const startY = Phaser.Math.Between(250, 600); // Random Y position within the road range

        const enemy = this.enem.create(startX, startY, enemyKey).setScale(1.8);
        (enemy.body as Phaser.Physics.Arcade.Body).setSize(30, 30); // Set collision box size

        enemy.flipX = true;

        enemy.play(`${enemyKey}-run`, true);
        // Add the enemy sprite to the array for future reference
        this.enemies.push(enemy);

        // Move the enemy across the screen
        this.tweens.add({
            targets: enemy,
            x: -50, // End outside the left edge of the screen
            duration: 20000, // Duration of the movement in milliseconds
            onComplete: () => {
                enemy.destroy(); // Destroy the enemy when it goes off-screen
                this.enemies = this.enemies.filter(e => e !== enemy); // Remove from array
            }
        });
    }

    spawnEnemyLoop() {
        this.time.addEvent({
            delay: 2000, // Delay between each enemy spawn in milliseconds
            callback: () => {
                const enemyKey = Phaser.Utils.Array.GetRandom(['enemy1', 'enemy2', 'enemy3']);
                this.createEnemy(enemyKey);
            },
            loop: true
        });
    }

    createAnimations() {
        const animations = [
            { key: 'champion1Sprite-idle', spriteSheet: 'champion1Sprite' },
            { key: 'champion2Sprite-idle', spriteSheet: 'champion2Sprite' },
            { key: 'champion3Sprite-idle', spriteSheet: 'champion3Sprite' },
            { key: 'champion4Sprite-idle', spriteSheet: 'champion4Sprite' },
            { key: 'champion5Sprite-idle', spriteSheet: 'champion5Sprite' },
            { key: 'champion6Sprite-idle', spriteSheet: 'champion6Sprite' },
            { key: 'enemy1-run', spriteSheet: 'enemy1' }, // Added enemy animations
            { key: 'enemy2-run', spriteSheet: 'enemy2' },
            { key: 'enemy3-run', spriteSheet: 'enemy3' },
        ];

        animations.forEach(({ key, spriteSheet }) => {
            if (!this.anims.exists(key)) {
                this.anims.create({
                    key,
                    frames: this.anims.generateFrameNumbers(spriteSheet),
                    frameRate: 6,
                    repeat: -1
                });
            }
        });
    }

    setupDragging() {
        this.input.on('dragstart', this.handleDragStart, this);
        this.input.on('drag', this.handleDrag, this);
        this.input.on('dragend', this.handleDragEnd, this);
        this.input.on('pointermove', this.handlePointerMove, this);
    }
    
    handleDragStart(pointer: Phaser.Input.Pointer, portrait: Phaser.GameObjects.Sprite) {
        const spriteKey = portrait.getData('sprite');
        if (spriteKey) {
            // Create and add the sprite to the world
            const sprite = this.physics.add.sprite(portrait.x, portrait.y, spriteKey).setScale(1);
            sprite.setData('portrait', portrait);
            sprite.play(`${spriteKey}-idle`); // Play idle animation
    
            // Store the sprite on the portrait for later reference
            portrait.setData('spriteInstance', sprite);
        }
    
        // Hide the portrait while dragging
        portrait.setVisible(false);
    }
    
    handleDrag(pointer: Phaser.Input.Pointer, portrait: Phaser.GameObjects.Sprite, dragX: number, dragY: number) {
        const sprite = portrait.getData('spriteInstance');
        if (sprite) {
            sprite.x = dragX; // Update X position during drag
            sprite.y = dragY; // Update Y position during drag
        }
    }
    
    handleDragEnd(pointer: Phaser.Input.Pointer, portrait: Phaser.GameObjects.Sprite) {
        const sprite = portrait.getData('spriteInstance');
        if (!sprite) return;
    
        // Find the circle where the sprite is dropped using collision detection
        const nearestCircle = this.physics.overlapRect(sprite.getBounds().x, sprite.getBounds().y, sprite.width, sprite.height)
            .find((overlap) => overlap.gameObject.getData('id')?.startsWith('circle'));
    
        if (nearestCircle && !nearestCircle.gameObject.getData('occupied')) {
            this.snapToCircle(sprite, nearestCircle.gameObject as Phaser.Physics.Arcade.Sprite);
        } else {
            this.resetChampionPosition(sprite, portrait);
        }
    }
    
    handlePointerMove(pointer: Phaser.Input.Pointer) {
        if (pointer.isDown && !pointer.event.target) {
            const delta = pointer.velocity.x / 10; // Adjust scrolling speed
            this.scrollContainer.x += delta; // Scroll container horizontally
            // Clamp scroll container position to keep it within bounds
            this.scrollContainer.x = Phaser.Math.Clamp(this.scrollContainer.x, -this.scrollContainer.width + (this.sys.game.config.width as number), 0);
        }
    }
    
    snapToCircle(sprite: Phaser.GameObjects.Sprite, circle: Phaser.Physics.Arcade.Sprite) {
        // Clear previous circle if any
        const previousCircle = sprite.getData('currentCircle');
        if (previousCircle) {
            previousCircle.setData('occupied', false);
            previousCircle.clearTint();
        }
    
        // Snap to the new circle
        sprite.x = circle.x;
        sprite.y = circle.y;
        circle.setData('occupied', true);
        circle.setTint(0x808080); // Change the circle color to gray
        sprite.setData('currentCircle', circle);
    
        // Ensure the sprite remains visible
        sprite.setVisible(true);
    }
    
    resetChampionPosition(sprite: Phaser.GameObjects.Sprite, portrait: Phaser.GameObjects.Sprite) {
        // Destroy the sprite and show the portrait
        sprite.destroy();
        portrait.setVisible(true);
        portrait.x = portrait.getData('initialX');
        portrait.y = portrait.getData('initialY');
    }
    
    
}

export default PlayScene;

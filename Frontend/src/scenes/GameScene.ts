import Phaser from 'phaser';
import BaseScene from './BaseScene';

class GameScene extends BaseScene {
    constructor() {
        super("GameScene");
    }
    
    create() {
        super.create();
        this.createBackground();
    }

    createBackground() {
        this.add.image(0, 0, 'bgBridge').setOrigin(0, 0).setDisplaySize(this.sys.game.config.width as number, this.sys.game.config.height as number);
    }
    
}

export default GameScene;

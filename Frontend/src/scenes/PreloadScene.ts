import Phaser from 'phaser';
import { getChampions } from '../services/api';

class PreloadScene extends Phaser.Scene {
    constructor() {
        super("PreloadScene");
    }

    async preload() {
        this.load.image("bgBridge", "assets/Background/4/castle-bridge.png");
        this.load.image("champion1", "assets/Champs/Mog/Mog.png");
        this.load.image("champion2", "assets/Champs/Jazz/Jazz.png");
        this.load.image("champion3", "assets/Champs/Miina/Miina.png");
        this.load.image("champion4", "assets/Champs/Orodruin/Orodruin.png");
        this.load.image("champion5", "assets/Champs/Aeryn/Aeryn.png");
        this.load.image("champion6", "assets/Champs/Cross/Cross.png");
        this.load.image("championSelectUIBrown", "assets/UI/Fantasy Wooden GUI  Free/Fantasy Wooden GUI  Free/PNG/UI board Large  stone.png");
        this.load.image("championSelectUITan", "assets/UI/PXUI-Basic (1)/PXUI-Basic/panel_Example1.png");
        this.load.image('pauseButton', 'assets/UI/Button/pauseBtn.png');
        this.load.image('uiPainel', 'assets/UI/Button/pauseBtn.png');
        // this.load.image('uiPainel2', 'assets/UI/painel/panelInset_beige.png');
        // this.load.image('uiPainel3', 'assets/UI/painel/panelInset_beigeLight.png');
        this.load.image('uiPainelEmptyBar', 'assets/UI/painel/emptyBar.png');
        this.load.image('uiPainelBarLeft', 'assets/UI/painel/barYellow_horizontalLeft.png');
        this.load.image('uiPainelBarMid', 'assets/UI/painel/barYellow_horizontalMid.png');
        this.load.image('uiPainelBarRight', 'assets/UI/painel/barYellow_horizontalRight.png');
        // Carregar outros assets aqui
        try {
            const champions = await getChampions();
            champions.forEach(async (champion: any, index: number) => {
                try {
                    this.load.image(`champion${index + 1}`, );
                } catch (error) {
                    console.error('Error loading image for champion:', champion.name, error);
                }
            });
        } catch (error) {
            console.error('Error loading champions:', error);
        }

        this.load.spritesheet("champion1Sprite", "assets/Champs/Mog/Sprites/Idle.png", {
            frameWidth: 128,
            frameHeight: 128
        });

        this.load.spritesheet("champion2Sprite", "assets/Champs/Jazz/Huntress/Sprites/Idle.png", {
            frameWidth: 150,
            frameHeight: 150
        });

        this.load.spritesheet("champion3Sprite", "assets/Champs/Miina/Huntress/Sprites/Idle.png", {
            frameWidth: 100,
            frameHeight: 100
        });

        this.load.spritesheet("champion4Sprite", "assets/Champs/Orodruin/Sprites/Idle.png", {
            frameWidth: 231,
            frameHeight: 190
        });

        this.load.spritesheet("champion5Sprite", "assets/Champs/Aeryn/Sprites/Idle.png", {
            frameWidth: 128,
            frameHeight: 128
        });

        this.load.spritesheet("champion6Sprite", "assets/Champs/Cross/Sprites/Idle.png", {
            frameWidth: 64,
            frameHeight: 64
        });

        this.load.spritesheet("circle", "assets/UI/Circle/Circle.png", {
            frameWidth: 144,
            frameHeight: 144
        });

        this.load.spritesheet("levelUpButton", "assets/UI/Button/buttonLong_blue.png", {
            frameWidth: 190,
            frameHeight: 49
        });

        this.load.spritesheet("levelUpButtonPressed", "assets/UI/Button/buttonLong_blue_pressed.png", {
            frameWidth: 190,
            frameHeight: 49
        });

        this.load.spritesheet("frameChamps", "assets/UI/Frames/frame.png", {
            frameWidth: 190,
            frameHeight: 49
        });

        this.load.spritesheet("enemy1", "assets/Enemies/Skeleton/Walk.png", {
            frameWidth: 150,
            frameHeight: 150
        });
        this.load.spritesheet("enemy2", "assets/Enemies/Mushroom/Run.png", {
            frameWidth: 150,
            frameHeight: 150
        });
        this.load.spritesheet("enemy3", "assets/Enemies/Goblin/Run.png", {
            frameWidth: 150,
            frameHeight: 150
        });
    }

    create() {
        this.scene.start("PlayScene");
    }

    createBackground() {
        this.add.image(0, 0, 'bgBridge').setOrigin(0, 0).setDisplaySize(this.sys.game.config.width as number, this.sys.game.config.height as number);
    }
}

export default PreloadScene;

import { DataTypes } from 'sequelize';
import { sequelize } from '../database/conecta.js';

export const Scenario = sequelize.define('scenario', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  name: {
    type: DataTypes.STRING(255),
    allowNull: false,
  },
  description: {
    type: DataTypes.TEXT,
  },
  assets: {
    type: DataTypes.JSON,
    allowNull: false,
  },
}, {
  tableName: 'scenarios',
  timestamps: true,
});

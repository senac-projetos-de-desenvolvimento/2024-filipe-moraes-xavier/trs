import { DataTypes } from 'sequelize';
import { sequelize } from '../database/conecta.js';

export const Mission = sequelize.define('mission', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  name: {
    type: DataTypes.STRING(255),
    allowNull: false,
  },
  description: {
    type: DataTypes.TEXT,
  },
  scenario_id: {
    type: DataTypes.INTEGER,
    references: {
      model: 'scenarios',
      key: 'id',
    },
  },
  type: {
    type: DataTypes.STRING(50), // 'Principal' or 'Secundária'
  },
  reward: {
    type: DataTypes.TEXT,
  },
  assets: {
    type: DataTypes.JSON,
    allowNull: false,
  },
}, {
  tableName: 'missions',
  timestamps: true,
});

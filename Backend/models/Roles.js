import { DataTypes } from 'sequelize';
import { sequelize } from '../database/conecta.js';

export const Role = sequelize.define('role', {
  id : {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  name: {
    type: DataTypes.STRING(40),
    allowNull: false
  }
},{
    tableName: 'roles',
    timestamps: true,
});
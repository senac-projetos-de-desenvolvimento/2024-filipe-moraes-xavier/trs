import { DataTypes } from 'sequelize';
import { sequelize } from '../database/conecta.js';

export const Antagonist = sequelize.define('antagonist', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  type: {
    type: DataTypes.STRING(50),
  },
  assets: {
    type: DataTypes.JSON,
    allowNull: false,
  },
}, {
  tableName: 'antagonists',
  timestamps: true,
});

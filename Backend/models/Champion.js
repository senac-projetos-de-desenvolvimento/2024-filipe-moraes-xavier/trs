import { DataTypes } from 'sequelize';
import { sequelize } from '../database/conecta.js';

export const Champion = sequelize.define('champion', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  unique_ability: {
    type: DataTypes.TEXT,
  },
  assets: {
    type: DataTypes.JSON,
    allowNull: false,
  },
}, {
  tableName: 'champions',
  timestamps: true,
});

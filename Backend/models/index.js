import { sequelize } from '../database/conecta.js';
import { Scenario } from './Scenario.js';
import { Character } from './Character.js';
import { Champion } from './Champion.js';
import { Antagonist } from './Antagonist.js';
import { Mission } from './Mission.js';
import { ChampionMission } from './ChampionMission.js';
import { Item } from './Item.js';
import { ChampionInventory } from './ChampionInventory.js';

// Associações
Scenario.hasMany(Mission, { foreignKey: 'scenario_id' });

Character.hasOne(Champion, { foreignKey: 'character_id' });
Character.hasOne(Antagonist, { foreignKey: 'character_id' });

Champion.belongsTo(Character, { foreignKey: 'character_id' });
Champion.belongsToMany(Mission, { through: ChampionMission, foreignKey: 'champion_id' });
Champion.belongsToMany(Item, { through: ChampionInventory, foreignKey: 'champion_id' });

Antagonist.belongsTo(Character, { foreignKey: 'character_id' });

Mission.belongsTo(Scenario, { foreignKey: 'scenario_id' });
Mission.belongsToMany(Champion, { through: ChampionMission, foreignKey: 'mission_id' });

Item.belongsToMany(Champion, { through: ChampionInventory, foreignKey: 'item_id' });

// Exportando modelos e sequelize
export {
  sequelize,
  Scenario,
  Character,
  Champion,
  Antagonist,
  Mission,
  ChampionMission,
  Item,
  ChampionInventory,
};

import { DataTypes } from 'sequelize';
import { sequelize } from '../database/conecta.js';

export const ChampionMission = sequelize.define('champion_mission', {
  mission_id: {
    type: DataTypes.INTEGER,
    references: {
      model: 'missions',
      key: 'id',
    },
  },
  champion_id: {
    type: DataTypes.INTEGER,
    references: {
      model: 'champions',
      key: 'id',
    },
  },
}, {
  tableName: 'champion_missions',
  timestamps: true,
});
import { DataTypes } from 'sequelize';
import { sequelize } from '../database/conecta.js';

export const Character = sequelize.define('character', {
  id : {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  name: {
    type: DataTypes.STRING(40),
    allowNull: false
  },
  race: {
    type: DataTypes.STRING(40),
    allowNull: false
  },
  gender: {
    type: DataTypes.STRING(40),
    allowNull: false
  },
  age: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  type: {
    type: DataTypes.STRING(50),
    allowNull: false, // 'Champion' or 'Antagonist'
  },
  tendencies: {
    type: DataTypes.STRING(40),
    allowNull: false
  },
  classes: {
    type: DataTypes.STRING(40),
    allowNull: false
  },
  abilities: {
    type: DataTypes.JSON,
    allowNull: false
  },
  level: {
    type: DataTypes.INTEGER,
    allowNull: false,
    defaultValue: 1
  },
  affiliation: {
    type: DataTypes.STRING(40),
    allowNull: false
  },
  stats: {
    type: DataTypes.JSON,
    allowNull: false
  },
  bio: {
    type: DataTypes.TEXT,
    allowNull: false
  },
  assets: {
    type: DataTypes.JSON,
    allowNull: false,
  },
  is_active: {
    type: DataTypes.INTEGER,
    defaultValue: 1
  }
},{
    tableName: 'characters',
    timestamps: true,
});
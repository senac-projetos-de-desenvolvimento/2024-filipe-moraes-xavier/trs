import { DataTypes } from 'sequelize';
import { sequelize } from '../database/conecta.js';

export const ChampionInventory = sequelize.define('champion_inventory', {
  champion_id: {
    type: DataTypes.INTEGER,
    references: {
      model: 'champions',
      key: 'id',
    },
  },
  item_id: {
    type: DataTypes.INTEGER,
    references: {
      model: 'items',
      key: 'id',
    },
  },
}, {
  tableName: 'champion_inventories',
  timestamps: true,
});
import { Champion } from "../models/Champion.js";

export async function championIndex(req, res) {
    try {
        const champions = await Champion.findAll();
        res.status(200).json(champions);
    } catch (error) {
        res.status(400).send(error);
    }
}

export async function championCreate(req, res) {
    const { name, age, race, gender, tendencies, classes, abilities, level, affiliation, stats, bio, is_active} = req.body;

    if (!name || !age || !race || !gender || !tendencies || !classes || !abilities || !level || !affiliation || !stats || !bio || !is_active) {
        res.status(400).json("Erro... Informe nome, idade, raça, genero, tendencia, classe, habilidades, level, affiliações, status, bio e se está ativo.");
        return;
    }
    try {
        const champion = await Champion.create({
            name, age, race, gender, tendencies, classes, abilities, level, affiliation, stats, bio, is_active
        });
        res.status(201).json(champion);
    } catch (error) {
        res.status(400).send(error);
    }
}

export async function championUpdate(req, res) {
    const { id } = req.params;
    const { name, age, race, gender, tendencies, classes, abilities, level, affiliation, stats, bio, is_active} = req.body;

    if (!name || !age || !race || !gender || !tendencies || !classes || !abilities || !level || !affiliation || !stats || !bio || !is_active) {
        res.status(400).json("Erro... Informe nome, idade, raça, genero, tendencia, classe, habilidades, level, affiliações, status, bio e se está ativo.");
        return;
    }
    try {
        const champion = await Champion.update({
            name, age, race, gender, tendencies, classes, abilities, level, affiliation, stats, bio, is_active
        }, {
            where: { id }
        });
        res.status(201).json(champion);
    } catch (error) {
        res.status(400).send(error);
    }
}

export async function championDelete(req, res) {
    const { id } = req.params
    
    try {
        await Champion.destroy({
            where: { id }
        })
        res.status(200).json({ msg: "Ok! Campeão removido com sucesso" })
    } catch (error) {
        res.status(400).send(error)
    }
}
import { Character } from '../models/Character.js';
import path from 'path';
import { fileURLToPath } from 'url';

// Obter o nome do arquivo atual e o diretório
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

export async function characterIndex(req, res) {
    try {
        const characters = await Character.findAll();
        res.status(200).json(characters);
    } catch (error) {
        res.status(400).send(error);
    }
}

export async function characterCreate(req, res) {
    const { name, race, gender, age, type, tendencies, classes, abilities, level, affiliation, stats, bio, assets, is_active } = req.body;

    if (!name || !race || !gender || !age || !type || !tendencies || !classes || !abilities || !level || !affiliation || !stats || !bio || !assets || !is_active) {
        res.status(400).json("Erro... Informe todas as informações para criar o personagem.");
        return;
    }
    try {
        const character = await Character.create({
            name, race, gender, age, type, tendencies, classes, abilities, level, affiliation, stats, bio, assets, is_active
        });
        res.status(201).json(character);
    } catch (error) {
        res.status(400).send(error);
    }
}

export async function characterUpdate(req, res) {
    const { id } = req.params;
    const { name, race, gender, age, type, tendencies, classes, abilities, level, affiliation, stats, bio, assets, is_active } = req.body;

    if (!name || !race || !gender || !age || !type || !tendencies || !classes || !abilities || !level || !affiliation || !stats || !bio || !assets || !is_active) {
        res.status(400).json("Erro... Informe todas as informações para criar o personagem.");
        return;
    }
    try {
        const character = await Character.update({
            name, race, gender, age, type, tendencies, classes, abilities, level, affiliation, stats, bio, assets, is_active
        }, {
            where: { id }
        });
        res.status(201).json(character);
    } catch (error) {
        res.status(400).send(error);
    }
}

export async function characterDelete(req, res) {
    const { id } = req.params
    
    try {
        await Character.destroy({
            where: { id }
        })
        res.status(200).json({ msg: "Ok! Personagem removido com sucesso" })
    } catch (error) {
        res.status(400).send(error)
    }
}

export async function showPortrait(req,res) {
    const { championName } = req.params;

    // Construir o caminho completo para o arquivo de imagem
    const imagePath = path.join(__dirname, `../assets/Champs/${championName}/${championName}.png`);
    
    // Enviar o arquivo de imagem como resposta
    res.sendFile(imagePath, (err) => {
        if (err) {
            console.error('Error sending file:', err);
            res.status(404).send('Image not found');
        }
    });
}

export async function showSprite(req,res) {
    const { championName } = req.params;

    // Construir o caminho completo para o arquivo de imagem
    const imagePath = path.join(__dirname, `../assets/Champs/${championName}/Idle.png`);
    
    // Enviar o arquivo de imagem como resposta
    res.sendFile(imagePath, (err) => {
        if (err) {
            console.error('Error sending file:', err);
            res.status(404).send('Image not found');
        }
    });
}
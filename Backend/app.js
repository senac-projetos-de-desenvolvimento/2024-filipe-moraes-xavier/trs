import express from 'express';
import cors from "cors";
import { sequelize } from './models/index.js';
import routes from './routes.js';

import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const router = express.Router();

const app = express();
const port = 3004;

app.use(express.json());
app.use(cors());
app.use(routes);

app.get('/', (req, res) => {
  res.send('Taticas do reino de Senescau');
});

// Ensure correct headers for static files
app.use('/assets', (req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  next();
}, express.static(path.join(__dirname, '../assets')));

async function conecta_db() {
  try {
    await sequelize.authenticate();
    console.log('Conexão com Banco de Dados realizada com Sucesso');
    await sequelize.sync({ force: true });
    console.log("Banco de dados e tabelas criadas!");
  } catch (error) {
    console.error('Erro ao conectar o banco de dados:', error);
  }  
}
conecta_db();

app.listen(port, () => {
  console.log(`API de Produtos Rodando na Porta: ${port}`);
});

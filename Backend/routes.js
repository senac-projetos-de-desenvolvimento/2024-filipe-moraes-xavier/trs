import { Router } from "express";
import { characterIndex, characterCreate, characterUpdate, characterDelete, showPortrait, showSprite } from "./controllers/CharactersController.js";

const router = Router();

router.get("/personagens", characterIndex)
      .post("/personagens", characterCreate)
      .put("/personagens/:id", characterUpdate)
      .delete("/personagens/:id", characterDelete)
      .get("/personagens/:championName", showPortrait)
      .get("/personagens/sprite/:championName", showSprite);

export default router;